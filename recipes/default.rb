

bash "jenkins_install" do
  user "root"
  code <<-EOH
    wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
    sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
    sudo apt-get update
    touch /var/chef/cache/jenkins.lock
  EOH
  not_if {File.exists?("/var/chef/cache/jenkins.lock")}
end

package "jenkins" do
  action :install
end

service "jenkins" do
  supports :status => true, :restart => true, :stop => true
  action :start
end

bash "jenkins_restart" do
  user "root"
  code <<-EOH
    /etc/init.d/jenkins force-reload
    touch /var/chef/cache/jenkins_force.lock
  EOH
  action :nothing
  not_if {File.exists?("/var/chef/cache/jenkins_force.lock")}
end

template "/etc/default/jenkins" do
    path "/etc/default/jenkins"
    source "jenkins.erb"
    owner "root"
    group "root"
    mode "0755"
    notifies :run, "bash[jenkins_restart]", :immediately
    #notifies :force-reload, resources(:service => "jenkins")
end


bash "jenkins_ssh_plugin" do
  user "root"
  cwd "/var/lib/jenkins"
  code <<-EOH
    wget http://updates.jenkins-ci.org/latest/ssh.hpi
  EOH
  notifies :restart, resources(:service => "jenkins")
  not_if {File.exists?("/var/lib/jenkins/ssh.hpi")}
end



=begin
uwsgi_port=8888
nginx_port=8889

template "/etc/nginx/sites-available/jenkins.nginx.conf" do
  path "/etc/nginx/sites-available/jenkins.nginx.conf"
  source "jenkins.nginx.conf.erb"
  owner "root"
  group "root"
  mode "0644"
  notifies :reload, resources(:service => "nginx")
  variables :nginx_port => nginx_port, :uwsgi_port => uwsgi_port
end

link "/etc/nginx/sites-enabled/jenkins.nginx.conf" do
  to "/etc/nginx/sites-available/jenkins.nginx.conf"
end
service "nginx"
=end














